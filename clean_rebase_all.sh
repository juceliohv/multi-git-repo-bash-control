#!/bin/bash

source $MULT_BASH_COMP/_include_color_control.sh

echo -n "${fcin}${bld}${bprl}";

BN=$1

echo "#######################################################";
echo "###                                                 ###";
echo "###      CLEAN AND REBASE GIT REPOSITORIES          ###";
echo "###                                                 ###";
echo "#######################################################";
#echo -n "${reset}";

for f in *; do
    if [ -d "$f/.git" ]; then
        # Will not run if no directories are available
        
	echo "${fcin}${bld}${bprl}";
        echo	
	echo "#######################################################";
	echo "                CLEANING $f REPOSITORY                 ";
	echo "#######################################################";
	#echo "${reset}"

	cd "$f"
	
	rm * -fR
		
	cd ..
    fi
done
echo -n "${reset}";
echo "${bell}";
./checkout_branch.sh $BN
