#!/bin/bash

#source $MULT_BASH_COMP/_include_color_control.sh

#echo "${fcin} ${bld} ${bprl}"; 

echo "#######################################################";
echo "###                                                 ###";
echo "###        NPM INSTALLING GIT REPOSITORIES          ###";
echo "###                                                 ###";
echo "#######################################################";
echo "";
npm version
echo "";

for f in *; do
    if [ -f "$f/package.json" ]; then
        # Will not run if no directories are available
        
#	echo "${fylw} ${bld} ${bprl}";
	echo "#######################################################";
	echo "             NPM INSTALLING $f REPOSITORY              ";
	echo "#######################################################";
	
	cd "$f"
	npm --verbose install nodemon -g
	cd ..
    fi
done
#echo "${reset}";
#echo "${bell} ${bell}";
