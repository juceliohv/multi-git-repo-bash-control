#!/bin/bash

source $MULT_BASH_COMP/_include_color_control.sh

BN=$1;

echo -n "${fcin}${bld}${bprl}"
echo "#######################################################";
echo "###                                                 ###";
echo "###                TAG GIT REPOSITORIES             ###";
echo "###                                                 ###";
echo "#######################################################";
echo -n "${reset}";

for f in *; do
    if [ -d "$f/.git" ]; then
        # Will not run if no directories are available
        
	echo "${fylw}${bld}${bprl}"
        echo	
	echo "#######################################################";
	echo "                TAGGING $f REPOSITORY                  ";
	echo "#######################################################";
	echo "${reset}"

	cd "$f"
	
	git tag "$BN$f" 

	git push origin --tags
	
	cd ..
    fi
done
echo "${bell}"

