#!/bin/bash

source $MULT_BASH_COMP/_include_color_control.sh


echo -n "${fcin}${bld}${bprl}"
echo "#######################################################";
echo "###                                                 ###";
echo "###             DIFF FROM GIT REPOSITORIES          ###";
echo "###                                                 ###";
echo "#######################################################";
echo -n "${reset}";

for f in *; do
    if [ -d "$f/.git" ]; then
        # Will not run if no directories are available
        
	echo "${fylw}${bld}${bprl}"
        echo	
	echo "#######################################################";
	echo "               DIFF FROM $f REPOSITORY                 ";
	echo "#######################################################";
	echo "${reset}"

	cd "$f"
	
	git diff
	
	cd ..
    fi
done
echo "${bell}"

