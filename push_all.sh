#!/bin/bash

source $MULT_BASH_COMP/_include_color_control.sh

echo -n "${fcin}${bld}${bprl}"
echo "#######################################################";
echo "###                                                 ###";
echo "###               PUSH GIT REPOSITORIES             ###";
echo "###                                                 ###";
echo "#######################################################";
echo -n "${reset}";

for f in *; do
    if [ -d "$f/.git" ]; then
        # Will not run if no directories are available
        
	echo "${fylw}${bld}${bprl} " 
        echo	
	echo "#######################################################";
	echo "                PUSHING $f REPOSITORY                  ";
	echo "#######################################################";
	echo "${reset}"

	cd "$f"
	
	git push && git fetch && git pull && git status
	
	cd ..
    fi
done
echo "${bell}"

