#!/bin/bash

source $MULT_BASH_COMP/_include_color_control.sh

BN=$1;
OB=$2;
CT=$3;

echo -n "${fcin}${bld}${bprl}"
echo "#######################################################";
echo "###                                                 ###";
echo "###             UPDATING GIT REPOSITORIES           ###";
echo "###                                                 ###";
echo "#######################################################";
echo -n "${reset}";

for f in *; do
    if [ -d "$f/.git" ]; then
        # Will not run if no directories are available
        
	echo "${fylw}${bld}${bprl}"
        echo	
	echo "#######################################################";
	echo "              UPDATING $f REPOSITORY                   ";
	echo "#######################################################";
	echo "${reset}"

	cd "$f"
	
	git reset --hard
	git checkout $CT $BN
	git fetch
	if [ -n $OB ]; then
		git pull origin $OB
	fi

	if [ -z $OB ]; then
		git pull origin $BN
	fi
	
	cd ..
    fi
done
echo "${bell}"

