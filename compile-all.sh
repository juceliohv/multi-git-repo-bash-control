#!/bin/bash

#source $MULT_BASH_COMP/_include_color_control.sh

#echo "${fcin} ${bld} ${bprl}"; 

echo "#######################################################";
echo "###                                                 ###";
echo "###             COMPILING GIT REPOSITORIES          ###";
echo "###                                                 ###";
echo "#######################################################";
echo "";
java -version
echo "";
mvn -version
echo "";

for f in *; do
    if [ -f "$f/pom.xml" ]; then
        # Will not run if no directories are available
        
#	echo "${fylw} ${bld} ${bprl}";
	echo "#######################################################";
	echo "             COMPILING $f REPOSITORY                   ";
	echo "#######################################################";
	
	cd "$f"
	mvn clean && mvn install 
	cd ..
    fi
done
#echo "${reset}";
#echo "${bell} ${bell}";