#!/bin/bash

source $MULT_BASH_COMP/_include_color_control.sh

MESSAGE=$1

echo -n "${fcin}${bld}${bprl}"
echo "#######################################################";
echo "###                                                 ###";
echo "###             COMMIT GIT REPOSITORIES             ###";
echo "###                                                 ###";
echo "#######################################################";
echo -n "${reset}";

for f in *; do
    if [ -d "$f/.git" ]; then
        # Will not run if no directories are available
        
	echo "${fylw}${bld}${bprl} " 
        echo	
	echo "#######################################################";
	echo "              COMMITING $f REPOSITORY                  ";
	echo "#######################################################";
	echo "${reset}"

	cd "$f"
	
	git add . && git commit -m \'"$MESSAGE"\'
	
	cd ..
    fi
done
echo "${bell}"

