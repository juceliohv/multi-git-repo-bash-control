#!/bin/bash

fred=`tput setaf 1`
fgrn=`tput setaf 2`
fylw=`tput setaf 3`
fblu=`tput setaf 4`
fprl=`tput setaf 5`
fcin=`tput setaf 6`
fwht=`tput setaf 7`
fcln=`tput setaf 9`

bred=`tput setab 1`
bgrn=`tput setab 2`
bylw=`tput setab 3`
bblu=`tput setab 4`
bprl=`tput setab 5`
bcin=`tput setab 6`
bwht=`tput setab 7`
bcln=`tput setab 9`

bell=`tput bel`

bld=`tput bold`
dim=`tput dim`

reset=`tput sgr0`
